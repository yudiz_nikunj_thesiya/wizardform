import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { StateMachineProvider, createStore } from "little-state-machine";
import PersonalInfo from "../components/PersonalInfo";
import SetPassword from "../components/SetPassword";
import Results from "../components/Results";

createStore({});

const Form = () => {
	return (
		<StateMachineProvider>
			<Router>
				<Routes>
					<Route path="/" element={<PersonalInfo />} />
					<Route path="/setpassword" element={<SetPassword />} />
					<Route path="/awesome" element={<Results />} />
				</Routes>
			</Router>
		</StateMachineProvider>
	);
};

export default Form;
