import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { MdOutlineChevronLeft } from "react-icons/md";
import { BsFillCheckCircleFill } from "react-icons/bs";
import { useStateMachine } from "little-state-machine";
import updateAction from "../actions/updateAction";
import "../styles/results.scss";
import { Link, useNavigate } from "react-router-dom";

const Results = () => {
	let navigate = useNavigate();
	const { actions, state } = useStateMachine({ updateAction });
	const { handleSubmit, reset } = useForm();
	const onReset = () => {
		reset(
			actions.updateAction({
				name: "",
				username: "",
				email: "",
				mobile: "",
				gender: "",
				password: "",
				confirmpassword: "",
				isHuman: "",
			})
		);
		navigate("/", { replace: true });
	};
	useEffect(() => {
		if (state.name === "" || state.password === "") {
			navigate("/");
		}
	}, []);
	return (
		<form className="form3">
			<div className="form3__header">
				<Link to="/setpassword" className="form3__header--left">
					<MdOutlineChevronLeft />
				</Link>
			</div>
			<div className="form3__step-3">
				<div className="checkIcon">
					<BsFillCheckCircleFill className="icon" />
					<span className="label">Awesome!</span>
				</div>
				<div className="desc">
					Your Data is Submitted. Please Check below. You can change it by
					clicking on back button and resubmit it.
				</div>
				<table className="table">
					<tr className="table__field">
						<th className="lbl">Name</th>
						<td className="value">{state.name}</td>
					</tr>
					<tr className="table__field">
						<th className="lbl">Username</th>
						<td className="value">{state.username}</td>
					</tr>
					<tr className="table__field">
						<th className="lbl">Email</th>
						<td className="value">{state.email}</td>
					</tr>
					<tr className="table__field">
						<th className="lbl">Mobile</th>
						<td className="value">{state.mobile}</td>
					</tr>
					<tr className="table__field">
						<th className="lbl">Gender</th>
						<td className="value">{state.gender}</td>
					</tr>
					<tr className="table__field">
						<th className="lbl">Password</th>
						<td className="value">{state.password}</td>
					</tr>
				</table>
			</div>
			<button
				type="button"
				className="form3__btn"
				onClick={handleSubmit(onReset)}
			>
				<span>Go to Home</span>
			</button>
		</form>
	);
};

export default Results;
