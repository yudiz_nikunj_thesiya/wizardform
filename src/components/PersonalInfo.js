import React from "react";
import { HiOutlineMail } from "react-icons/hi";
import { ImPhone } from "react-icons/im";
import { useForm } from "react-hook-form";
import { FiUser } from "react-icons/fi";
import { BsArrowRight } from "react-icons/bs";
import { useStateMachine } from "little-state-machine";
import updateAction from "../actions/updateAction";
import { useNavigate } from "react-router-dom";
import "../styles/personalinfo.scss";

const PersonalInfo = () => {
	let navigate = useNavigate();
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm();
	const { actions, state } = useStateMachine({ updateAction });
	const onSubmit = (data) => {
		actions.updateAction(data);
		navigate("/setpassword");
	};

	return (
		<form className="form1" onSubmit={handleSubmit(onSubmit)}>
			<div className="form1__header">
				<div className="header">
					<span className="step">Step 1 of 2</span>
					<span className="heading">Personal Information</span>
				</div>
			</div>
			<div className="form1__step-1">
				<div className="input__container">
					<div className={errors.name ? `input_err` : `input_default`}>
						<FiUser className="icon" />
						<input
							type="text"
							className=""
							name="name"
							placeholder="Your Name"
							{...register("name", {
								required: true,
								minLength: 6,
								maxLength: 20,
								pattern: /^[^\s]+(?:$|.*[^\s]+$)/,
							})}
							defaultValue={state.name}
						/>
					</div>
					<span className="err">
						{errors.name?.type === "required" && "Name is required"}
						{errors.name?.type === "minLength" &&
							"Name should be atleast 6 characters."}
						{errors.name?.type === "maxLength" &&
							"Name should not be greater than 20."}
						{errors.name?.type === "pattern" &&
							"Please remove extra white spaces."}
					</span>
				</div>

				<div className="input__container">
					<div className={errors.username ? `input_err` : `input_default`}>
						<FiUser className="icon" />
						<input
							type="text"
							className=""
							name="username"
							placeholder="Username"
							{...register("username", {
								required: true,
								minLength: 6,
								maxLength: 20,
								pattern: /^[a-z0-9_.]+$/,
							})}
							defaultValue={state.username}
						/>
					</div>
					<div className="err">
						{errors.username?.type === "required" && "Username is required"}
						{errors.username?.type === "maxLength" &&
							"Username should not be greater than 20."}
						{errors.username?.type === "minLength" &&
							"Username should contains atleast 6 characters."}
						{errors.username?.type === "pattern" &&
							"Usernames only contains letters, numbers, underscores, and periods."}
					</div>
				</div>

				<div className="input__container">
					<div className={errors.email ? `input_err` : `input_default`}>
						<HiOutlineMail className="icon" />
						<input
							type="text"
							className=""
							name="email"
							placeholder="Email"
							{...register("email", {
								required: true,
								pattern: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/,
							})}
							defaultValue={state.email}
						/>
					</div>
					<span className="err">
						{errors.email?.type === "required" && "Email is required"}
						{errors.email?.type === "pattern" && "Please enter valid Email"}
					</span>
				</div>

				<div className="input__container">
					<div className={errors.mobile ? `input_err` : `input_default`}>
						<ImPhone className="icon" />
						<input
							type="number"
							className=""
							name="mobilenumber"
							placeholder="Mobile Number"
							{...register("mobile", {
								required: true,
								minLength: 10,
								maxLength: 10,
							})}
							defaultValue={state.mobile}
						/>
					</div>
					<span className="err">
						{errors.mobile?.type === "required" && "Mobile is required"}
						{errors.mobile?.type === "minLength" &&
							"Mobile Number should be 10 Digits."}
						{errors.mobile?.type === "maxLength" &&
							"Mobile Number should be 10 Digits."}
					</span>
				</div>
				<div className="input__container">
					<div className="gender">
						<span className="gender__label">Select Gender</span>
						<div className="gender__radio">
							<div className="gender__radio--male">
								<input
									type="radio"
									name="gender"
									value="male"
									checked={state.gender === "male" ? true : null}
									{...register("gender", {
										required: true,
									})}
								/>
								<span>Male</span>
							</div>
							<div className="gender__radio--male">
								<input
									type="radio"
									name="gender"
									value="female"
									checked={state.gender === "female" ? true : null}
									{...register("gender", {
										required: true,
									})}
								/>
								<span>Female</span>
							</div>
						</div>
					</div>
					<span className="err">
						{errors.gender?.type === "required" && "Gender is required"}
					</span>
				</div>
			</div>
			<button type="submit" className="form1__btn">
				<span>Next Step</span>
				<BsArrowRight />
			</button>
		</form>
	);
};

export default PersonalInfo;
