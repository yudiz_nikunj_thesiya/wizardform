import React, { useEffect, useState } from "react";
import { BiLock } from "react-icons/bi";
import { MdOutlineChevronLeft, MdOutlineChevronRight } from "react-icons/md";
import { BsFillEyeFill, BsFillEyeSlashFill } from "react-icons/bs";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useStateMachine } from "little-state-machine";
import { useForm } from "react-hook-form";
import "../styles/setpassword.scss";
import { Link } from "react-router-dom";
import updateAction from "../actions/updateAction";
import { useNavigate } from "react-router-dom";

const SetPassword = () => {
	const formSchema = Yup.object().shape({
		password: Yup.string()
			.required("Password is mendatory")
			.min(7, "Password must be at 7 char long"),
		confirmpassword: Yup.string()
			.required("Password is mendatory")
			.oneOf([Yup.ref("password")], "Passwords does not match"),
		isHuman: Yup.bool().oneOf([true], "Please accept terms & conditions."),
	});

	const [showPass, setShowPass] = useState(false);
	const [showConPass, setShowConPass] = useState(false);

	const passwordOptions = { resolver: yupResolver(formSchema) };
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm(passwordOptions);
	const { actions, state } = useStateMachine({ updateAction });
	let navigate = useNavigate();

	const onSubmit = (data) => {
		actions.updateAction(data);
		navigate("/awesome");
	};
	useEffect(() => {
		if (state.name === "") {
			navigate("/");
		}
	}, []);

	return (
		<form className="form2" onSubmit={handleSubmit(onSubmit)}>
			<div className="form2__header">
				<Link to="/" className="form2__header--left">
					<MdOutlineChevronLeft />
				</Link>

				<div className="header">
					<span className="step">Step 2 of 2</span>
					<span className="heading">Password</span>
				</div>

				<Link to="" className="form2__header--right-disabled">
					<MdOutlineChevronRight />
				</Link>
			</div>
			<div className="form2__step-2">
				<div className="input__container">
					<div className={errors.password ? `input_err` : `input_default`}>
						<BiLock className="icon" />
						<input
							type={showPass === true ? "text" : "password"}
							className=""
							placeholder="Password"
							{...register("password")}
							defaultValue={state.password}
						/>
						{showPass === true ? (
							<BsFillEyeSlashFill
								className="icon"
								onClick={() => setShowPass(false)}
							/>
						) : (
							<BsFillEyeFill
								className="icon"
								onClick={() => setShowPass(true)}
							/>
						)}
					</div>
					<span className="err">{errors.password?.message}</span>
				</div>
				<div className="input__container">
					<div
						className={errors.confirmpassword ? `input_err` : `input_default`}
					>
						<BiLock className="icon" />
						<input
							type={showConPass === true ? "text" : "password"}
							className=""
							placeholder="Confirm Password"
							{...register("confirmpassword", {
								required: true,
								minLength: 7,
							})}
							defaultValue={state.confirmpassword}
						/>
						{showConPass === true ? (
							<BsFillEyeSlashFill
								className="icon"
								onClick={() => setShowConPass(false)}
							/>
						) : (
							<BsFillEyeFill
								className="icon"
								onClick={() => setShowConPass(true)}
							/>
						)}
					</div>
					<span className="err">{errors.confirmpassword?.message}</span>
				</div>
				<div className="check__container">
					<div className="check">
						<input
							type="checkbox"
							{...register("isHuman")}
							checked={state.isHuman === true ? true : null}
						/>
						<span className="check__text">
							I Accept <span className="link">Terms & Condition</span>.
						</span>
					</div>
					<span className="err">{errors.isHuman?.message}</span>
				</div>
			</div>
			<button type="submit" className="form2__btn">
				<span>Submit</span>
			</button>
		</form>
	);
};

export default SetPassword;
