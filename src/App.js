import React from "react";
import Form from "./sections/Form";
import "./styles/app.scss";

function App() {
	return (
		<div className="form__container">
			<Form />
		</div>
	);
}

export default App;
